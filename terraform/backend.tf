######## RESOURCE: S3
######## https://docs.aws.amazon.com/AmazonS3/latest/userguide/Welcome.html
######## https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket
######## https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket



terraform {
  backend "s3" {
    #bucket = "djimi-gilles-vero"
    bucket = "djimi-gilles-vero-test"
    key    = "vero/"
    region = "eu-west-3"
  }
}