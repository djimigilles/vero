######## RESOURCE: VPC
######## https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Subnets.html
######## https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/vpc#attributes-reference

resource "aws_vpc" "principal_vpc" {
  cidr_block = var.vpc_cidr
  enable_dns_hostnames = true

  tags = {
    Name = "principal-vpc"
  }
}