######## RESOURCE Route Table
######## https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Route_Tables.html
######## https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/route



resource "aws_route_table" "principal_public" {
  vpc_id = aws_vpc.principal_vpc.id

  route = [
    {
      cidr_block                 = "0.0.0.0/0"
      gateway_id                 = aws_internet_gateway.principal_igw.id
      nat_gateway_id             = ""
      carrier_gateway_id         = ""
      destination_prefix_list_id = ""
      egress_only_gateway_id     = ""
      instance_id                = ""
      ipv6_cidr_block            = ""
      local_gateway_id           = ""
      network_interface_id       = ""
      transit_gateway_id         = ""
      vpc_endpoint_id            = ""
      vpc_peering_connection_id  = ""
    },
  ]

  tags = {
    Name = "public"
  }
}



resource "aws_route_table" "principal_private" {
  count = length(var.paris_az)
  vpc_id = aws_vpc.principal_vpc.id

  route = [
    {
      cidr_block                 = "0.0.0.0/0"
      nat_gateway_id             = aws_nat_gateway.nat[count.index].id
      carrier_gateway_id         = ""
      destination_prefix_list_id = ""
      egress_only_gateway_id     = ""
      gateway_id                 = ""
      instance_id                = ""
      ipv6_cidr_block            = ""
      local_gateway_id           = ""
      network_interface_id       = ""
      transit_gateway_id         = ""
      vpc_endpoint_id            = ""
      vpc_peering_connection_id  = ""
    },
  ]

  tags = {
    Name = "route-private-${var.paris_az[count.index]}"
  }
}



resource "aws_route_table_association" "public" {
  count = length(var.paris_az)
  subnet_id      = aws_subnet.public_subnet[count.index].id
  route_table_id = aws_route_table.principal_public.id
}


resource "aws_route_table_association" "private" {
  count = length(var.paris_az)
  subnet_id      = aws_subnet.private_subnet[count.index].id
  route_table_id = aws_route_table.principal_private[count.index].id
}