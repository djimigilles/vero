######## RESOURCE: Internet Gateway
######## https://docs.aws.amazon.com/vpc/latest/userguide/VPC_Internet_Gateway.html
######## https://registry.terraform.io/providers/hashicorp/aws/2.43.0/docs/resources/internet_gateway


resource "aws_internet_gateway" "principal_igw" {
  vpc_id = aws_vpc.principal_vpc.id

  tags = {
    Name = "principal-igw"
  }
}