######## RESOURCE: AWS lb
######## https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb#access_logs

######## RESOURCE: AWS listener
######## https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_listener

######## RESOURCE: AWS Target Group
######## https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_target_group

######## RESOURCE: AWS Target Group Attachment
######## https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lb_target_group_attachment


resource "aws_lb" "app_alb" {
  name               = "app-lb"
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.app_alb_sg.id]
  subnets            = aws_subnet.public_subnet.*.id

  enable_deletion_protection = false


  tags = {
    Environment = "production"
    Name        = "app-alb"
    App         = "greylog"
  }
}


resource "aws_lb_listener" "app_alb_listener" {
  load_balancer_arn = aws_lb.app_alb.arn
  port              = "8080"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.app_tg.arn
  }
}


resource "aws_lb_target_group" "app_tg" {
  name     = "app-alb-tg"
  port     = 8080
  protocol = "HTTP"
  vpc_id   = aws_vpc.principal_vpc.id

  health_check {
    protocol = "HTTP"
    port     = "traffic-port"
    path     = "/"
    matcher  = "200"
  }
}


resource "aws_security_group" "app_alb_sg" {
  name        = var.app_alb_sg_name
  description = "Allow incoming connections."

  vpc_id = aws_vpc.principal_vpc.id

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

}