######## RESOURCE: provider
######## https://registry.terraform.io/providers/hashicorp/aws/latest/docs
######## https://www.terraform.io/language/providers/requirements

terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}


provider "aws" {
  region  = "eu-west-3"
  profile = "terraform"
}