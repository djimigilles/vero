######## RESOURCE: Nat Gateway
######## https://docs.aws.amazon.com/vpc/latest/userguide/vpc-nat-gateway.html
######## https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/nat_gateway

resource "aws_nat_gateway" "nat" {
  count = length(var.paris_az)
  allocation_id = aws_eip.eip[count.index].id
  subnet_id     = aws_subnet.public_subnet[count.index].id

  tags = {
    Name = "nat-${var.paris_az[count.index]}"
  }

  depends_on = [aws_internet_gateway.principal_igw]
}