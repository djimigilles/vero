######## RESOURCE: RDS
######## https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Welcome.html
######## https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/db_instance
######## https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/rds_cluster_instance


resource "aws_db_subnet_group" "database-subnet-group" {
  name        = "database subnets"
  subnet_ids  = aws_subnet.private_subnet.*.id
  description = "Subnets for Database Instance"

  tags = {
    Name = "Database Subnets"
  }
}



resource "aws_rds_cluster" "postgresql" {
  cluster_identifier      = var.aurora_cluster_name
  engine                  = var.aurora_engine
  availability_zones      = var.paris_az
  database_name           = var.aurora_db_name
  master_username         = var.aurora_db_username
  master_password         = var.aurora_db_password
  backup_retention_period = 5
  preferred_backup_window = "07:00-09:00"
  db_subnet_group_name    = aws_db_subnet_group.database-subnet-group.name
  skip_final_snapshot     = true
  vpc_security_group_ids = [aws_security_group.rds_aurora_sg.id]
}


resource "aws_rds_cluster_instance" "postgresql" {
  count                = 3
  identifier           = "aurora-cluster-demo-${count.index}"
  cluster_identifier   = aws_rds_cluster.postgresql.id
  db_subnet_group_name = aws_db_subnet_group.database-subnet-group.name
  instance_class       = "db.r4.large"
  engine               = aws_rds_cluster.postgresql.engine
  engine_version       = aws_rds_cluster.postgresql.engine_version
}


resource "aws_security_group" "rds_aurora_sg" {
  name        = var.rds_aurora_sg_name
  description = "Allow incoming connections."

  vpc_id = aws_vpc.principal_vpc.id

  ingress {
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}