######## RESOURCE: Launch Template
######## https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/ec2-launch-templates.html
######## https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/launch_template


resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = var.pair_key
}


resource "aws_launch_template" "app_launch_template" {
  name = "vero"

  image_id = var.instance_ami

  instance_initiated_shutdown_behavior = "terminate"

  instance_type = var.instance_type

  key_name = "deployer-key"

  monitoring {
    enabled = true
  }

  network_interfaces {
    associate_public_ip_address = true
    security_groups = [aws_security_group.app_launch_template_sg.id]
  }
  

  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = var.instance_tag
    }
  }

  user_data = filebase64("${path.module}/vero.sh")

  depends_on = [aws_key_pair.deployer,]
}



resource "aws_security_group" "app_launch_template_sg" {
  name        = var.app_launch_template_sg
  description = "Allow incoming connections."

  vpc_id = aws_vpc.principal_vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    security_groups = [aws_security_group.app_alb_sg.id]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }
}